#include <iostream>

#include "MSBoardTextView.h"

#include "MinesweeperBoard.h"

MSBoardTextView::MSBoardTextView(MinesweeperBoard & Board): Plansza(Board) {}

void MSBoardTextView::display() const {
    for (int wiersz = 0; wiersz < height; wiersz++) {
        for (int kolumna = 0; kolumna < width; kolumna++) {
            std::cout << Plansza.getFieldInfo(wiersz, kolumna) << " ";
        }
        std::cout << std::endl;
    }
    std::cout << "----------------------------------------------" << std::endl;
}