#ifndef MSTextController_H_
#define MSTextController_H_
#include "MinesweeperBoard.h"

#include "MSBoardTextView.h"

class MSTextController {
    MinesweeperBoard & Plansza;
    int height = Plansza.getBoardHeight();
    int width = Plansza.getBoardWidth();

    MSBoardTextView & Okno;

public:
    MSTextController(MinesweeperBoard & Board, MSBoardTextView & Window);
    void play();
};

#endif
