#include "MSSFMLView.h"
#include "SFML/Graphics.hpp"
#include "SFML/Window.hpp"

MSSFMLView::MSSFMLView(MinesweeperBoard & b): board(b) {
    initComponents();
}

void MSSFMLView::initComponents(){
    initTextures();
    initSprites();
}

void MSSFMLView::initTextures(){
    for (int i=0; i<12; i++){
        textures[i].loadFromFile("D:/.projekty/sfml/textures/texture" + std::to_string(i) + ".png");
    }

    buttonTextures[0].loadFromFile("D:/.projekty/sfml/textures/saper.png");
    buttonTextures[1].loadFromFile("D:/.projekty/sfml/textures/lost.png");
    buttonTextures[2].loadFromFile("D:/.projekty/sfml/textures/win.png");
}

void MSSFMLView::initSprites(){
    int wys_planszy = board.getBoardHeight();
    int szer_planszy = board.getBoardWidth();

    sprites.resize(wys_planszy);
    for(int i=0; i<wys_planszy; i++){
        sprites[i].resize(szer_planszy);
    }

    for(int wiersz = 0; wiersz < wys_planszy; wiersz++){
        for(int kolumna = 0; kolumna < szer_planszy; kolumna++){
            int x,y;
            x = 50 + 20*kolumna;
            y = 50 + 20*wiersz;
            sprites[wiersz][kolumna].setPosition(x,y);
        }
    }
    buttonSprite.setTexture(buttonTextures[0]);
    buttonSprite.setPosition(0, 0);
}

void MSSFMLView::updateField(int wiersz, int kolumna){
    char stan_pola = board.getFieldInfo(wiersz, kolumna);
    switch(stan_pola){
        case 'F':
            sprites[wiersz][kolumna].setTexture(textures[11]);
            break;
        case '_':
            sprites[wiersz][kolumna].setTexture(textures[9]);
            break;
        case 'x':
            sprites[wiersz][kolumna].setTexture(textures[10]);
            break;
        case ' ':
            sprites[wiersz][kolumna].setTexture(textures[0]);
            break;
        case '1':
            sprites[wiersz][kolumna].setTexture(textures[1]);
            break;
        case '2':
            sprites[wiersz][kolumna].setTexture(textures[2]);
            break;
        case '3':
            sprites[wiersz][kolumna].setTexture(textures[3]);
            break;
        case '4':
            sprites[wiersz][kolumna].setTexture(textures[4]);
            break;
        case '5':
            sprites[wiersz][kolumna].setTexture(textures[5]);
            break;
        case '6':
            sprites[wiersz][kolumna].setTexture(textures[6]);
            break;
        case '7':
            sprites[wiersz][kolumna].setTexture(textures[7]);
            break;
        case '8':
            sprites[wiersz][kolumna].setTexture(textures[8]);
            break;
        default:
            return;

    }
}

void MSSFMLView::draw(sf::RenderWindow & win) {
    for(int wiersz = 0; wiersz < board.getBoardHeight(); wiersz++){
        for(int kolumna = 0; kolumna < board.getBoardWidth(); kolumna++){
            updateField(wiersz, kolumna);
            win.draw(sprites[wiersz][kolumna]);
        }
    }
    win.draw(buttonSprite);
    if(board.getGameState()!= RUNNING){
        if(board.getGameState() == FINISHED_LOSS){
            buttonSprite.setTexture(buttonTextures[1]);
        }
        if(board.getGameState() == FINISHED_WIN){
            buttonSprite.setTexture(buttonTextures[2]);
        }
    }
}