#include <iostream>

#include <cstdlib>

#include <ctime>

#include <math.h>

#include "MinesweeperBoard.h"

MinesweeperBoard::MinesweeperBoard(int height, int width, GameMode mode): board(width, height) //KONSTRUKTOR
{
    this -> width = width;
    this -> height = height;
    this -> mode = mode;
    this -> play = RUNNING;
    for (int wiersz = 0; wiersz < height; wiersz++) {
        for (int kolumna = 0; kolumna < width; kolumna++) {
            board[wiersz][kolumna].hasMine = 0;
            board[wiersz][kolumna].hasFlag = 0;
            board[wiersz][kolumna].isRevealed = 0;
        }
    }
    double minesMultip;
    switch (mode) {
        //----------------
        case EASY:
            minesMultip = 0.1;
            break;
            //----------------
        case NORMAL:
            minesMultip = 0.2;
            break;
            //----------------
        case HARD:
            minesMultip = 0.3;
            break;
            //----------------
        case DEBUG:
            minesMultip = 0;
            break;
            //----------------
        default:
            minesMultip = 0;
            break;
    }
    this -> ilosc_min = ceil(height * width * minesMultip);

    if (mode == DEBUG)
    {
        for (int wiersz = 0; wiersz < height; wiersz++) {
            for (int kolumna = 0; kolumna < width; kolumna++) {
                if (wiersz == kolumna) {
                    board[wiersz][kolumna].hasMine = 1;
                }
                if (wiersz == 0) {
                    board[wiersz][kolumna].hasMine = 1;
                }
                if (kolumna == 0 && wiersz % 2 == 0) {
                    board[wiersz][kolumna].hasMine = 1;
                }
            }
        }
    }
    srand(time(NULL));
    int miny = ilosc_min;
    while (miny > 0) {
        int wiersz_2 = std::rand() % height;
        int kolumna_2 = std::rand() % width;
        if (board[wiersz_2][kolumna_2].hasMine == 1) {
            continue;
        }
        board[wiersz_2][kolumna_2].hasMine = 1;
        miny -= 1;
    }
}

void MinesweeperBoard::debug_display() const
{
    for (int wiersz = 0; wiersz < height; wiersz++) {
        for (int kolumna = 0; kolumna < width; kolumna++) {
            std::cout << "[";
            if (board[wiersz][kolumna].hasMine == 1)
                std::cout << "M";
            else std::cout << ".";

            if (board[wiersz][kolumna].isRevealed == 1)
                std::cout << "o";
            else std::cout << ".";

            if (board[wiersz][kolumna].hasFlag == 1)
                std::cout << "f";
            else std::cout << ".";
            std::cout << "]";
        }
        std::cout << std::endl;
    }
}

bool MinesweeperBoard::pole_poza_mapa(int wiersz, int kolumna) const {
    if (wiersz < 0 || kolumna < 0 || wiersz > (height - 1) || kolumna > (width - 1)) {
        return true;
    }
    return false;
}

int MinesweeperBoard::getBoardWidth() const
{
    return width;
}

int MinesweeperBoard::getBoardHeight() const
{
    return height;
}

int MinesweeperBoard::getMineCount() const
{
    return ilosc_min;
}

bool MinesweeperBoard::hasFlag(int wiersz, int kolumna) const
{
    if (pole_poza_mapa(wiersz, kolumna) == 1 || board[wiersz][kolumna].hasFlag == 0 || board[wiersz][kolumna].isRevealed == 1) {
        return false;
    }
    return true;
}

void MinesweeperBoard::toggleFlag(int wiersz, int kolumna) {
    if( play == FINISHED_WIN || play == FINISHED_LOSS) {return;}
    if( pole_poza_mapa(wiersz, kolumna) == 1) {return;}
    if( board[wiersz][kolumna].isRevealed == 1) {return;}

    if (board[wiersz][kolumna].hasFlag == 1) {
        board[wiersz][kolumna].hasFlag = 0;
    } else board[wiersz][kolumna].hasFlag = 1;
}

void MinesweeperBoard::revealField(int wiersz, int kolumna) {
    if(pole_poza_mapa(wiersz, kolumna == 1 )) {return;}
    if( play == FINISHED_WIN || play == FINISHED_LOSS ) {return;}
    if( board[wiersz][kolumna].isRevealed == 1 || board[wiersz][kolumna].hasFlag == 1 ) {return;}
    if(pierwsza_akcja == 0 && mode != DEBUG && board[wiersz][kolumna].hasMine == 1)
    {
        int mina = 1;
        while (mina > 0) {
            int wiersz_2 = std::rand() % height;
            int kolumna_2 = std::rand() % width;
            if (board[wiersz_2][kolumna_2].hasMine == 1) {
                continue;
            }
            board[wiersz_2][kolumna_2].hasMine = 1;
            mina -= 1;
        }
        board[wiersz][kolumna].hasMine = 0;
    }

    if (board[wiersz][kolumna].hasMine == 0) {
        board[wiersz][kolumna].isRevealed = 1;
        if(getFieldInfo(wiersz, kolumna) == ' ')
        {
            if (pole_poza_mapa(wiersz - 1, kolumna - 1) == 0){
                revealField(wiersz-1, kolumna-1);
            }
            if (pole_poza_mapa(wiersz - 1, kolumna) == 0){
                revealField(wiersz-1, kolumna);
            }
            if (pole_poza_mapa(wiersz - 1, kolumna + 1) == 0){
                revealField(wiersz-1, kolumna+1);
            }
            if (pole_poza_mapa(wiersz, kolumna - 1) == 0){
                revealField(wiersz, kolumna-1);
            }
            if (pole_poza_mapa(wiersz, kolumna + 1) == 0){
                revealField(wiersz, kolumna+1);
            }
            if (pole_poza_mapa(wiersz + 1, kolumna - 1) == 0){
                revealField(wiersz+1, kolumna-1);
            }
            if (pole_poza_mapa(wiersz + 1, kolumna) == 0){
                revealField(wiersz+1, kolumna);
            }
            if (pole_poza_mapa(wiersz + 1, kolumna + 1) == 0){
                revealField(wiersz+1, kolumna+1);
            }
        }
    }

    if (board[wiersz][kolumna].hasMine == 1) {
        board[wiersz][kolumna].isRevealed = 1;
        this -> play = FINISHED_LOSS;
    }
    pierwsza_akcja = 1;
}

bool MinesweeperBoard::isRevealed(int wiersz, int kolumna) const {
    if (pole_poza_mapa(wiersz, kolumna) == 0 && board[wiersz][kolumna].isRevealed == 1) {
        return true;
    }
    return false;
}

GameState MinesweeperBoard::getGameState() const {
    if (play == FINISHED_LOSS) return FINISHED_LOSS;
    int licznik = 0;
    for (int wiersz = 0; wiersz < height; wiersz++) {
        for (int kolumna = 0; kolumna < width; kolumna++) {
            if (board[wiersz][kolumna].isRevealed == 0) {
                licznik += 1;
            }
        }
    }
    if (licznik == ilosc_min) {
        return FINISHED_WIN;
    }
    return RUNNING;
}

int MinesweeperBoard::countMines(int wiersz, int kolumna) const { //FUNKCJA liczy miny dookola pola
    if (pole_poza_mapa(wiersz, kolumna) == 1 || board[wiersz][kolumna].isRevealed == 0)
        return -1;

    int miny_dookola = 0;

    if (pole_poza_mapa(wiersz - 1, kolumna - 1) == 0) {
        if (board[wiersz - 1][kolumna - 1].hasMine == 1) miny_dookola += 1;
    }

    if (pole_poza_mapa(wiersz - 1, kolumna) == 0) {
        if (board[wiersz - 1][kolumna].hasMine == 1) miny_dookola += 1;
    }

    if (pole_poza_mapa(wiersz - 1, kolumna + 1) == 0) {
        if (board[wiersz - 1][kolumna + 1].hasMine == 1) miny_dookola += 1;
    }

    if (pole_poza_mapa(wiersz, kolumna - 1) == 0) {
        if (board[wiersz][kolumna - 1].hasMine == 1) miny_dookola += 1;
    }

    if (pole_poza_mapa(wiersz, kolumna + 1) == 0) {
        if (board[wiersz][kolumna + 1].hasMine == 1) miny_dookola += 1;
    }

    if (pole_poza_mapa(wiersz + 1, kolumna - 1) == 0) {
        if (board[wiersz + 1][kolumna - 1].hasMine == 1) miny_dookola += 1;
    }

    if (pole_poza_mapa(wiersz + 1, kolumna) == 0) {
        if (board[wiersz + 1][kolumna].hasMine == 1) miny_dookola += 1;
    }

    if (pole_poza_mapa(wiersz + 1, kolumna + 1) == 0) {
        if (board[wiersz + 1][kolumna + 1].hasMine == 1) miny_dookola += 1;
    }

    return miny_dookola;
}

char MinesweeperBoard::getFieldInfo(int wiersz, int kolumna) const { //FUNKCJA zwraca wartosc pola

    if (MinesweeperBoard::pole_poza_mapa(wiersz, kolumna) == 1) //Pole poza planszą
        return '#';
    if (board[wiersz][kolumna].hasFlag == 1 && board[wiersz][kolumna].isRevealed == 0) //Nieodkryte pole z flagą
        return 'F';
    if (board[wiersz][kolumna].hasFlag == 0 && board[wiersz][kolumna].isRevealed == 0) //Nieodkryte pole bez flagi
        return '_';
    if (board[wiersz][kolumna].isRevealed == 1 && board[wiersz][kolumna].hasMine == 1) //Pole odkryte z miną
        return 'x';
    if (board[wiersz][kolumna].isRevealed == 1 && MinesweeperBoard::countMines(wiersz, kolumna) == 0) //Pole odkryte bez min dookoła
        return ' ';
    if (board[wiersz][kolumna].isRevealed == 1 && MinesweeperBoard::countMines(wiersz, kolumna) != 0) //Pole odkryte z X minami dookoła
    {
        if (MinesweeperBoard::countMines(wiersz, kolumna) == 1) return '1';
        if (MinesweeperBoard::countMines(wiersz, kolumna) == 2) return '2';
        if (MinesweeperBoard::countMines(wiersz, kolumna) == 3) return '3';
        if (MinesweeperBoard::countMines(wiersz, kolumna) == 4) return '4';
        if (MinesweeperBoard::countMines(wiersz, kolumna) == 5) return '5';
        if (MinesweeperBoard::countMines(wiersz, kolumna) == 6) return '6';
        if (MinesweeperBoard::countMines(wiersz, kolumna) == 7) return '7';
        if (MinesweeperBoard::countMines(wiersz, kolumna) == 8) return '8';
    }
    return '#';
}