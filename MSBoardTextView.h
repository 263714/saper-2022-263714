#ifndef MSBoardTextView_H_
#define MSBoardTextView_H_
#include "MinesweeperBoard.h"

class MSBoardTextView {
    MinesweeperBoard & Plansza;
    int height = Plansza.getBoardHeight();
    int width = Plansza.getBoardWidth();

public:
    MSBoardTextView(MinesweeperBoard & Board);
    void display() const;
};

#endif
