#ifndef MinesweeperBoard_H_
#define MinesweeperBoard_H_
#include "Array2D.h"

enum GameMode {
    DEBUG,
    EASY,
    NORMAL,
    HARD
};
enum GameState {
    RUNNING,
    FINISHED_WIN,
    FINISHED_LOSS
};

struct Field {
    bool hasMine;
    bool hasFlag;
    bool isRevealed;
};

class MinesweeperBoard {
    Array2D < Field > board;

    int width;
    int height;
    int ilosc_min;
    bool pierwsza_akcja = 0;
    GameMode mode;
    GameState play;
    bool pole_poza_mapa(int row, int col) const;

public:
    void debug_display() const;

    MinesweeperBoard(int height, int width, GameMode mode);


    int getBoardWidth() const;
    int getBoardHeight() const;
    int getMineCount() const;


    int countMines(int row, int col) const;


    bool hasFlag(int row, int col) const;


    void toggleFlag(int row, int col);


    void revealField(int row, int col);


    bool isRevealed(int row, int col) const;


    GameState getGameState() const;


    char getFieldInfo(int row, int col) const;
};

#endif