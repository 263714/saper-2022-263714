#include "SFML/Graphics.hpp"
#include "SFML/Window.hpp"

#include <iostream>
#include "MSSFMLView.h"
#include "MinesweeperBoard.h"
#include "MSBoardTextView.h"
#include "MSTextController.h"
#include <cmath>


int main()
{
    MinesweeperBoard board(16, 16, EASY);
    MSSFMLView view(board);

    sf::RenderWindow window(sf::VideoMode(800, 600), "Minesweeper");
    window.setVerticalSyncEnabled(false);
    window.setFramerateLimit(60);

    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        }
        view.localPosition = sf::Mouse::getPosition(window);
        if(sf::Mouse::isButtonPressed(sf::Mouse::Left))
        {
            int wiersz = ceil((view.localPosition.y-50)/20);
            int kolumna = ceil((view.localPosition.x-50)/20);
            board.revealField(wiersz, kolumna);

        }
        if(sf::Mouse::isButtonPressed(sf::Mouse::Right)){
            int wiersz = ceil((view.localPosition.y-50)/20);
            int kolumna = ceil((view.localPosition.x-50)/20);
            board.toggleFlag(wiersz, kolumna);
        }
        window.clear();
        view.draw(window);
        window.display();
    }

    return 0;
}
