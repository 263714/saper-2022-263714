#include <iostream>

#include "MinesweeperBoard.h"

#include "MSBoardTextView.h"

#include "MSTextController.h"


MSTextController::MSTextController(MinesweeperBoard & Board, MSBoardTextView & Window): Plansza(Board), Okno(Window) {}

void MSTextController::play() {
    while (true) {
        Okno.display();

        int opcja = 0;
        int wiersz = 0;
        int kolumna = 0;
        while (opcja < 1 || opcja > 2) {
            std::cout << "1. Postaw Flagę." << std::endl;
            std::cout << "2. Odkryj Pole." << std::endl;
            std::cin >> opcja;
        }
        std::cout << "Podaj które pole" << std::endl;
        std::cin >> wiersz >> kolumna;

        switch (opcja) {

            case 1:
                Plansza.toggleFlag(wiersz, kolumna);
                break;

            case 2:
                Plansza.revealField(wiersz, kolumna);
                break;

            default:
                break;

        }

        if (Plansza.getGameState() != RUNNING) {
            std::cout << "Gra skończona: ";
            if (Plansza.getGameState() == FINISHED_WIN) {
                std::cout << "Wygrana";
            }
            if (Plansza.getGameState() == FINISHED_LOSS) {
                std::cout << "Przegrana";
            }
            break;
        }
    }
}