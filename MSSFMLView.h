#ifndef MSSFMLVIEW_H_
#define MSSFMLVIEW_H_

#include "SFML/Graphics.hpp"
#include "SFML/Window.hpp"
#include "MinesweeperBoard.h"
#include <array>
#include <vector>

class MSSFMLView {
    MinesweeperBoard & board;

    sf::Sprite buttonSprite;
    std::array<sf::Texture, 3> buttonTextures;
    std::vector<std::vector<sf::Sprite>> sprites;
    std::array<sf::Texture, 12> textures;

    void initTextures();
    void initSprites();
    void initComponents();
    void updateField(int row, int col);

public:
    explicit MSSFMLView(MinesweeperBoard & b);
    sf::Vector2i localPosition;
    void draw(sf::RenderWindow & win);
};

#endif